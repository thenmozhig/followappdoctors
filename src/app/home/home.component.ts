import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ClaimProfileService } from '../services/claim-profile.service';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
  listArray: any = [];
  Passingdata = "Thenmozhi Govindasmay";
  constructor(private _service: ClaimProfileService, private _router: Router) {

  }

  ngOnInit() {
    this.listArray = ["Jan", "Feb", "March", "april", "May", "june", "july", "august", "sep", "oct", "nov", "dec"];
    // this._service.myMethod("ghjhfjhjkdfjkfdjklfd")
    localStorage.setItem('searchdoctors', '')
    localStorage.setItem('searchcity', '')


  }

  updateSearchdata() {
    this._service.searchCity.next($("#searchcitycontrolhome").val());
    this._service.searchDoctor.next($("#searchcontrolhome").val());
    // this._router.navigate(['/doctors']);
    localStorage.setItem('searchdoctors', $("#searchcontrolhome").val())
    localStorage.setItem('searchcity', $("#searchcitycontrolhome").val())
  }

  ngAfterViewInit() {
    var controller = this;
    $(document).keypress(function (event) {
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if (keycode == '13') {
        controller._router.navigate(['/doctors']);
      }
    });
  }
}
