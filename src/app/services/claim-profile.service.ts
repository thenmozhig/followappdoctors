import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClaimProfileService {

  myMethod$: Observable<any>;
  private myMethodSubject = new Subject<any>();

  public searchDoctor: BehaviorSubject<any> = new BehaviorSubject<any>('');
  public searchCity: BehaviorSubject<any> = new BehaviorSubject<any>('');

  constructor() {
    this.myMethod$ = this.myMethodSubject.asObservable();
  }

  myMethod(data:any) {
    // console.log(data);
     // I have data! Let's return it so subscribers can use it!
    // we can do stuff with data if we want
    this.myMethodSubject.next(data);
  }
  getDataService() {
    return this.myMethodSubject;
  }
}
