import { TestBed } from '@angular/core/testing';

import { ClaimProfileService } from './claim-profile.service';

describe('ClaimProfileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClaimProfileService = TestBed.get(ClaimProfileService);
    expect(service).toBeTruthy();
  });
});
