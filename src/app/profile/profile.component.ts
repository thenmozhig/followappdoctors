import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { ClaimProfileService } from '../services/claim-profile.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, ErrorStateMatcher } from '@angular/material';
import * as $ from 'jquery';
import { FormControl, Validators, FormGroupDirective, NgForm, FormGroup } from '@angular/forms';
import { map } from 'rxjs/operators';
import { MouseEvent } from '@agm/core';
import * as CryptoJS from 'crypto-js';


export interface DialogData {
  msgotp: string;
  mobile: string;
  otp: string;
  regid: string;
  phoneno: string;
  doctorid: string;
}


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {
  regid: any;
  doctorid: any;
  doctorname: string;
  doctorimage: string = "https://nalamhcit.com/imadoctorsimages/doctor_icon.jpg";
  doctordetail: any;
  education: any;
  mobileValidation: any;
  doctorMobileNo: any;
  OTP: any;
  zoom: any = 10;

  infoWindowVar: any;

  msgotp: string;
  mobile: string = '';

  address: any = "";

  lat: number;
  lng: number;
  currentText: any;

  public userProfileData: Array<any>;

  constructor(private _route: ActivatedRoute, private _httpclient: HttpClient, private _claimService: ClaimProfileService, public dialog: MatDialog, private _router: Router) {
    if (this._route.snapshot.params.regno) {
      this.regid = CryptoJS.AES.decrypt(this._route.snapshot.params.regno, 'FollowAPP').toString(CryptoJS.enc.Utf8);
      // this.regid = this._route.snapshot.params.regno;
      console.log(this._route.snapshot.params.regno);

    }
    else {
      this.regid = '';
    }
    if (this._route.snapshot.params.id) {
      console.log(this._route.snapshot.params.id);
      // this.doctorid = this._route.snapshot.params.id;
      this.doctorid = CryptoJS.AES.decrypt(this._route.snapshot.params.id, 'FollowAPP').toString(CryptoJS.enc.Utf8);
    }
    else {
      this.doctorid = '';
    }


    this._httpclient.post('https://dev.followapp.in:3000/GetIMADoctorDetailsOnRegisterNumberLive', { 'pRegNo': this.regid, 'pDoctorID': this.doctorid }).subscribe((res: any) => {
      if (res.status == 0) {
        console.log(res.response);
        this.doctorname = res.response.DoctorName.toLowerCase();
        this.address = res.response.Address;
        let educationArraytemp = res.response.Qualification != '' ? (this.IsValidJSONString(res.response.Qualification) ? JSON.parse(res.response.Qualification) : res.response.Qualification) : '';
        let educationArray = educationArraytemp.EducationalInfo;

        if (educationArray != '') {
          if (Array.isArray(educationArray)) {
            // educationArray.forEach((edu: any) => {
            //   edu.College = edu.College != '' ? edu.College + ' - ' : '';
            //   edu.University = edu.University && edu.University.includes('University') ? edu.University : edu.University + " University"
            //   let list = `${edu.Qualification} - ${edu.College} ${edu.University}, ${edu.YearofCompletion}`;
            //   this.education.push(list)
            // })
            this.education = educationArray;
          }
          else {
            if (res.response.Qualification != '') {
              // this.education.push(res.response.Qualification);
              var dataTempQualificationJSONArray = [];
              var dataTempQualificationJSON = {
                Qualification: res.response.Qualification,
                College: '',
                University: '',
                YearofCompletion: '',
                RegisteredDate: '',
              };
              dataTempQualificationJSONArray.push(dataTempQualificationJSON);
              this.education = dataTempQualificationJSONArray;

              // res.response.Qualification = res.response.Qualification;

            }
            else {
              // this.education.push('Not Available');
              this.education = '';
              res.response.Qualification = 'Not Available';

            }
          }
        }

        let QualList = []
        res.response.Qualification = res.response.Qualification != '' ? (this.IsValidJSONString(res.response.Qualification) ? JSON.parse(res.response.Qualification) : res.response.Qualification) : '';

        // res.response.Qualification = res.response.Qualification != '' ? JSON.parse(res.response.Qualification) : '';
        if (res.response.Qualification != '') {
          if (res.response.Qualification.EducationalInfo) {
            res.response.Qualification.EducationalInfo.forEach((qu: any) => {
              QualList.push(qu.Qualification)
            })
          }
          else {
            QualList.push(res.response.Qualification)
          }
        }

        res.response.qual = QualList.join(", ")
        if (res.response.Photo && res.response.Photo.includes('https')) {
          this.doctorimage = res.response.Photo
        } else {
          if (res.response.Photo) {
            this.doctorimage = `https://nalamhcit.com/imadoctorsimages/images/${res.response.Photo}`
          } else {
            this.doctorimage = "../../assets/images/ima_doctors_icon.png"
          }
        }
        // res.response.Address = res.response.Address ? res.response.Address : 'Not available'
        this.doctordetail = res.response;
        this.doctorMobileNo = res.response.ContactNo;
        this.mobileValidation = res.response.ContactNo.length;
        for (var i = 0; i < this.doctorMobileNo.length; i++) {
          if (i <= 2) {
            this.mobile = this.mobile + this.doctorMobileNo.charAt(i);
          }
          else {
            if (i >= 7) {
              this.mobile = this.mobile + this.doctorMobileNo.charAt(i);
            }
            else {
              this.mobile = this.mobile + "*";
            }
          }
        }
        var addressinfo = this.address;
        if (addressinfo != "") {
          let weatherurl = `https://dev.virtualearth.net/REST/v1/Locations?q=${addressinfo}&key=Aggu0kVUFhBedZTdp1CDt-d5dSppewSLl8zej4HCyTyQiEffLpC43VoxmfYe5coh`;
          let currentContext = this;

          currentContext._httpclient.get(weatherurl)
            .pipe(map(result => result))
            .subscribe((resultset: any) => {
              this.lat = resultset.resourceSets[0].resources[0].geocodePoints[0].coordinates[0];
              this.lng = resultset.resourceSets[0].resources[0].geocodePoints[0].coordinates[1];
              this.currentText = this.address;
            })
        }
      }
    })
  }

  ngOnInit() {
  }

  IsValidJSONString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  clickedMarker(infowindow, title) {
    this.currentText = title;
    if (infowindow.isOpen == true) {
      infowindow.close();
    }
    else {
      infowindow.open();
    }
    this.infoWindowVar = infowindow;
  }

  openDialog(): void {

    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '450px',
      disableClose: true,
      data: { mobile: this.mobile, msgotp: this.msgotp, regid: this.regid, phoneno: this.doctorMobileNo, doctorid: this.doctorid }
    });

  }
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    console.log(!!(control && control.invalid && (control.dirty || control.touched || isSubmitted)));
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'user-validation-dialog',
  templateUrl: 'user-validation-dialog.html',
})
export class DialogOverviewExampleDialog {



  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private _httpclient: HttpClient, private _router: Router) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  validationbyUser(): void {
    let OTPRandom: any = Math.floor(Math.random() * 1000000);
    this.data.otp = OTPRandom;
    let Message = "Dear User, Thank you for claiming your profile in https://followapp.in. Your OTP for registration is  " + OTPRandom + ". If this message is by error: Please report at https://followapp.in/?errorclaim:" + OTPRandom;
    this._httpclient.post('https://followapp.in:5000/SendSMS', { 'pMessage': Message, 'pMobileNumber': this.data.phoneno }).subscribe((res: any) => {
      if (res.status == 0) {
        $("#mobilenoverification").hide();
        $("#mobilenovalidation").show();
        $("#claimMblSubmit").hide();
        $("#claimProfileSubmit").show();
      }
    });

  }

  validateOTP(): void {
    let tempOTP = $("#otpInput").val();
    let orgOTP = this.data.otp.toString();
    if (tempOTP.length == orgOTP.length) {
      if (tempOTP == orgOTP) {
        $("#otperror").hide();
        $("#claimProfileSubmit").removeAttr('ng-reflect-disabled');
        $("#claimProfileSubmit").removeAttr('disabled');
      }
      else {
        $("#otperror").show();
        $("#claimProfileSubmit").attr('ng-reflect-disabled', false);
        $("#claimProfileSubmit").attr('disabled');
      }
    }
  }

  validateOTPSubmit() {
    let userEnteredOTP = $("#otpInput").val();
    let orgOTP = this.data.otp.toString();
    if (userEnteredOTP.length == orgOTP.length) {
      if (userEnteredOTP == orgOTP) {
        this.dialogRef.close();
        if (this.data.doctorid != '') {
          this._router.navigate(['/Claim_Profile',  CryptoJS.AES.encrypt(this.data.doctorid,'FollowAPP').toString()]);
        }
        else {
          this._router.navigate(['/claim_profile',  CryptoJS.AES.encrypt(this.data.regid,'FollowAPP').toString()]);
        }
      }
      else {
        $("#otperror").show();
        $("#claimProfileSubmit").attr('ng-reflect-disabled', false);
        $("#claimProfileSubmit").attr('disabled');
      }
    }
  }
}
