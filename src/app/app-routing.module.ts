import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { ProfileComponent } from './profile/profile.component';
import { ClaimProfileComponent } from './claim-profile/claim-profile.component';
import { AddDoctorsComponent } from './add-doctors/add-doctors.component';




const routes: Routes = [
  { path: '', redirectTo: '/search', pathMatch: 'full' },
  { path: 'search', component: HomeComponent },
  { path: 'doctors', component: SearchComponent },
  { path: 'add-doctors', component: AddDoctorsComponent },
  { path: 'doctors/profile/:id', component: ProfileComponent },
  { path: 'doctors/Profile/:regno', component: ProfileComponent },
  { path: 'claim_profile/:regno', component: ClaimProfileComponent },
  { path: 'Claim_Profile/:id', component: ClaimProfileComponent },
  { path: '**', redirectTo: '/search' }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {useHash: true}),
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    RouterModule,
    ReactiveFormsModule
  ]
})
export class AppRoutingModule { }


