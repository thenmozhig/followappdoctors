import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PageEvent } from '@angular/material';
import * as $ from 'jquery';
import { HomeComponent } from "../../app/home/home.component"
import { ClaimProfileService } from '../services/claim-profile.service';
import * as CryptoJS from 'crypto-js';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, AfterViewInit {
  doctorlist: any[];
  dataPassbyhome: any;
  dataPassbyhomecity: any;
  doctorListLength: any;
  // qual:any;
  pageEvent: PageEvent;
  tempRegno: any = '';

  @ViewChild(HomeComponent) homedata;

  isSearchDone = 0;
  constructor(private _http: HttpClient, private _service: ClaimProfileService) {
    $("#searchResultloader").show();
    $("#searchResultcontainer").hide();

    this._service.searchCity.subscribe(data => {
      this.dataPassbyhomecity = data;
      if (localStorage.searchcity == '') {
        localStorage.setItem('searchcity', data)
      }
    })
    this._service.searchDoctor.subscribe(data => {
      this.dataPassbyhome = data;
      if (localStorage.searchdoctors == '') {
        localStorage.setItem('searchdoctors', data)
      }
    })
    //  if (this.dataPassbyhome != "" || this.dataPassbyhomecity != '') {
    //     this.GetDoctorsbySearch(this.dataPassbyhome, this.dataPassbyhomecity, 10, 0);
    //   }
    if ((localStorage.searchcity != '' || localStorage.searchdoctors != '') && (localStorage.searchcity != undefined || localStorage.searchdoctors != undefined)) {
      this.dataPassbyhomecity = localStorage.searchcity;
      this.dataPassbyhome = localStorage.searchdoctors;

      this.GetDoctorsbySearch(this.dataPassbyhome, this.dataPassbyhomecity, 10, 0);
    }
    else {
      $("#searchResultloader").show();
      $("#searchResultcontainer").hide();
      this._http.post('https://followapp.in:5000/GetIMADoctorsWithPagination', { pLimit: 10, pOffset: 0 }).subscribe((res: any) => {
        if (res.status == 0) {
          $("#searchResultloader").hide();
          $("#searchResultcontainer").show();
          this.doctorListLength = res.doctorcount;
          res.response.forEach((response: any) => {
            response.Qualification = response.Qualification != '' ? (this.IsValidJSONString(response.Qualification) ? JSON.parse(response.Qualification) : response.Qualification) : '';
            response.State = response.State ? response.State : 'Not available'
            response.DoctorName = (response.DoctorName).toLowerCase();
            if (response.Photo) {
              if (response.Photo && (response.Photo).includes('https') == true) {
                response.ImagePath = response.Photo
              }
              else {
                response.ImagePath = "https://nalamhcit.com/imadoctorsimages/images/" + response.Photo;
              }
            }
            else {
              response.ImagePath = "../../assets/images/ima_doctors_icon.png"
            }

            let QualList = []
            if (response.Qualification != '' && response.Qualification != null) {
              response.Qualification.EducationalInfo.forEach((qu) => {
                QualList.push(qu.Qualification)
              })
            }
            // response.qual = QualList.join(", ")
            response.qual = QualList.length == 0 ? '' : QualList.join(", ")
            if (response.DoctorID != '') {
              var dataTempDoctorID = CryptoJS.AES.encrypt(response.DoctorID, 'FollowAPP').toString();
              response.DoctorID = dataTempDoctorID;
            }
            else {
              var dataTempRegNo = CryptoJS.AES.encrypt(response.RegNo, 'FollowAPP').toString();
              response.RegNo = dataTempRegNo;
            }

          })

          this.doctorlist = res.response
          this.doctorlist
        } else {
          this.doctorlist = []
          $("#searchResultloader").hide();
          $("#searchResultcontainer").show();
        }
      })
    }

  }

  ngOnInit() {

  }

  SearchDoctors() {
    if ($("#searchcontrol").val() != "" || $("#searchcitycontrol").val() != '') {
      localStorage.setItem('searchdoctors', $("#searchcontrol").val())
      localStorage.setItem('searchcity', $("#searchcitycontrol").val())

      this.GetDoctorsbySearch($("#searchcontrol").val(), $("#searchcitycontrol").val(), 10, 0);
    }
    else {
      $("#searchResultloader").show();
      $("#searchResultcontainer").hide();
      this._http.post('https://followapp.in:5000/GetIMADoctorsWithPagination', { pLimit: 10, pOffset: 0 }).subscribe((res: any) => {
        // console.log(res)
        if (res.status == 0) {
          this.isSearchDone = 0;
          this.doctorListLength = res.doctorcount;

          res.response.forEach((response: any) => {
            response.Qualification = response.Qualification != '' ? (this.IsValidJSONString(response.Qualification) ? JSON.parse(response.Qualification) : response.Qualification) : '';
            response.State = response.State ? response.State : 'Not available'
            response.DoctorName = (response.DoctorName).toLowerCase();
            if (response.Photo) {
              if (response.Photo && (response.Photo).includes('https') == true) {
                response.ImagePath = response.Photo
              }
              else {
                response.ImagePath = "https://nalamhcit.com/imadoctorsimages/images/" + response.Photo;
              }
            }
            else {
              response.ImagePath = "../../assets/images/ima_doctors_icon.png"
            }
            // response.qual 
            let QualList = []

            if (response.Qualification != '' && response.Qualification != null) {
              if (typeof res.response.Qualification != 'string') {
                var QualificationList = res.response.Qualification.EducationalInfo;
                for (let j = 0; j < QualificationList.length; j++) {
                  QualList.push(QualificationList[j].Qualification)
                }
              }
              else {
                QualList.push(res.response.Qualification)
              }
              // response.Qualification.EducationalInfo.forEach((qu) => {
              //   QualList.push(qu.Qualification)
              // })
            }

            response.qual = QualList.length == 0 ? '' : QualList.join(", ")
            if (response.DoctorID != '') {
              var dataTempDoctorID = CryptoJS.AES.encrypt(response.DoctorID, 'FollowAPP').toString();
              response.DoctorID = dataTempDoctorID;
            }
            else {
              var dataTempRegNo = CryptoJS.AES.encrypt(response.RegNo, 'FollowAPP').toString();
              response.RegNo = dataTempRegNo;
            }
          })
          $("#searchResultloader").hide();
          $("#searchResultcontainer").show();

          this.doctorlist = res.response

          this.doctorlist
        } else {
          this.doctorlist = []
          $("#searchResultloader").hide();
          $("#searchResultcontainer").show();
        }

      })
    }


  }

  ngAfterViewInit() {
    // this.dataPassbyhome = this.homedata.Passingdata;
    // console.log(this.dataPassbyhome);
    var controller = this;
    $("#searchcontrol").val(this.dataPassbyhome);
    $("#searchcitycontrol").val(this.dataPassbyhomecity)
    $(document).keypress(function (event) {
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if (keycode == '13') {
        controller.SearchDoctors();
      }
    });
   
  }


  handlePage(e) {
    if (this.isSearchDone == 0) {
      $("#searchResultloader").show();
      $("#searchResultcontainer").hide();
      this._http.post('https://followapp.in:5000/GetIMADoctorsWithPagination', { pLimit: e.pageSize, pOffset: (e.pageIndex * 5) }).subscribe((res: any) => {
        // console.log(res)
        if (res.status == 0) {
          this.doctorListLength = res.doctorcount;

          res.response.forEach((response: any) => {
            response.Qualification = response.Qualification != '' ? (this.IsValidJSONString(response.Qualification) ? JSON.parse(response.Qualification) : response.Qualification) : '';
            response.State = response.State ? response.State : 'Not available'
            response.DoctorName = (response.DoctorName).toLowerCase();
            if (response.Photo) {
              if (response.Photo && (response.Photo).includes('https') == true) {
                response.ImagePath = response.Photo
              }
              else {
                response.ImagePath = "https://nalamhcit.com/imadoctorsimages/images/" + response.Photo;
              }
            }
            else {
              response.ImagePath = "../../assets/images/ima_doctors_icon.png"
            }
            // response.qual 
            let QualList = []
            if (response.Qualification != '' && response.Qualification != null) {
              response.Qualification.EducationalInfo.forEach((qu) => {
                QualList.push(qu.Qualification)
              })
            }
            response.qual = QualList.length == 0 ? '' : QualList.join(", ")
            if (response.DoctorID != '') {
              var dataTempDoctorID = CryptoJS.AES.encrypt(response.DoctorID, 'FollowAPP').toString();
              response.DoctorID = dataTempDoctorID;
            }
            else {
              var dataTempRegNo = CryptoJS.AES.encrypt(response.RegNo, 'FollowAPP').toString();
              response.RegNo = dataTempRegNo;
            }
          })

          this.doctorlist = res.response
          $("#searchResultloader").hide();
          $("#searchResultcontainer").show();
          this.doctorlist
        } else {
          this.doctorlist = []
          $("#searchResultloader").hide();
          $("#searchResultcontainer").show();
        }
      })
    }

    else {
      this.GetDoctorsbySearch($("#searchcontrol").val(), $("#searchcitycontrol").val(), e.pageSize, (e.pageIndex * 5));
    }
  }


  IsValidJSONString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  GetDoctorsbySearch(Doctor, City, Limit, Offset) {
    $("#searchResultloader").show();
    $("#searchResultcontainer").hide();
    this._http.post('https://followapp.in:5000/GetIMADoctorsSearchWithPagination', { pDoctorname: Doctor, pCity: City, pLimit: Limit, pOffset: Offset }).subscribe((res: any) => {
      this.isSearchDone = 1;

      if (res.status == 0) {
        this.doctorListLength = res.response[1][0].doctorcount;
        if (res.response[0]) {
          for (let i = 0; i < res.response[0].length; i++) {
            res.response[0][i].Qualification = res.response[0][i].Qualification != '' ? (this.IsValidJSONString(res.response[0][i].Qualification) ? JSON.parse(res.response[0][i].Qualification) : res.response[0][i].Qualification) : '';
            res.response[0][i].State = res.response[0][i].State ? res.response[0][i].State : 'Not available'
            res.response[0][i].DoctorName = (res.response[0][i].DoctorName).toLowerCase();
            if (res.response[0][i].Photo) {
              if (res.response[0][i].Photo && (res.response[0][i].Photo).includes('https') == true) {
                res.response[0][i].ImagePath = res.response[0][i].Photo
              }
              else {
                res.response[0][i].ImagePath = "https://nalamhcit.com/imadoctorsimages/images/" + res.response[0][i].Photo;
              }
            }
            else {
              res.response[0][i].ImagePath = "../../assets/images/ima_doctors_icon.png"
            }
            let QualList = []
            if (res.response[0][i].Qualification != '') {
              if (res.response[0][i].Qualification.EducationalInfo) {
                var QualificationList = res.response[0][i].Qualification.EducationalInfo;
                for (let j = 0; j < QualificationList.length; j++) {
                  QualList.push(QualificationList[j].Qualification)
                }
              }
              else {
                QualList.push(res.response[0][i].Qualification)
              }

            }
            res.response[0][i].qual = QualList.length == 0 ? '' : QualList.join(", ")

            if (res.response[0][i].DoctorID != '') {
              var dataTempDoctorID = CryptoJS.AES.encrypt(res.response[0][i].DoctorID, 'FollowAPP').toString();
              res.response[0][i].DoctorID = dataTempDoctorID;
            }
            else {
              var dataTempRegNo = CryptoJS.AES.encrypt(res.response[0][i].RegNo, 'FollowAPP').toString();
              res.response[0][i].RegNo = dataTempRegNo;
            }

          }


          this.doctorlist = res.response[0]
          $("#searchResultloader").hide();
          $("#searchResultcontainer").show();
        }
      } else {
        this.doctorlist = []
        $("#searchResultloader").hide();
        $("#searchResultcontainer").show();
      }

    })
  }

}

