// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-add-doctors',
//   templateUrl: './add-doctors.component.html',
//   styleUrls: ['./add-doctors.component.css']
// })
// export class AddDoctorsComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }

import { Component, OnInit, Inject, NgZone, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ClaimProfileService } from '../services/claim-profile.service';
import * as $ from 'jquery';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { take } from 'rxjs/operators';
import * as CryptoJS from 'crypto-js';
import * as MessageDigest from 'angular-md5';


export interface DialogData {
  profile: any;
  hospital: string;
  address: string;
  timing: string;
  fees: string;
}

export interface Gender {
  value: string;
  viewValue: string;
}
export interface StateCodes {
  value: string;
  viewValue: string;
}
export interface FreeVisit {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-add-doctors',
  templateUrl: './add-doctors.component.html',
  styleUrls: ['./add-doctors.component.css']
})
export class AddDoctorsComponent implements OnInit {

  ClaimProfileFormGroup: FormGroup;
  Array_len: any;
  control: any;
  addRow: any;
  addInsRow: any;
  qualError: any = 0;
  insError: any = 0;
  urlForUserImageJSON: any;
  urlForUserImage: any;

  regid: any;
  doctorname: any;
  education: string[] = [];
  doctorimage: any;
  doctordetail: any;
  doctorid: string;

  // Dialog Variables
  profile: any;
  hospital: string;
  address: string;
  timing: string;
  fees: string;
  aeskey = "Na1am TECHNOBEES";

  EducationTextareaText: any;

  header = new HttpHeaders();

  genders: Gender[] = [
    { value: 'Male', viewValue: 'Male' },
    { value: 'Female', viewValue: 'Female' },
    { value: 'Others', viewValue: 'Others' }
  ];
  statecodes: StateCodes[] = [
    { value: 'IN-TN', viewValue: 'IN-TN' },
    { value: 'IN-KL', viewValue: 'IN-KL' },
    { value: 'IN-AP', viewValue: 'IN-AP' },
    { value: 'IN-BR', viewValue: 'IN-BR' },
    { value: 'IN-TG', viewValue: 'IN-TG' },
    { value: 'IN-PY', viewValue: 'IN-PY' }
  ];
  freevisits: FreeVisit[] = [
    { value: '1', viewValue: '1' },
    { value: '2', viewValue: '2' },
    { value: '3', viewValue: '3' },
    { value: '4', viewValue: '4' },
    { value: '5', viewValue: '5' },
    { value: '6', viewValue: '6' },
    { value: '7', viewValue: '7' }
  ];

  public userProfileData: any;

  @ViewChild('autosize') autosize: CdkTextareaAutosize;

  constructor(private _fb: FormBuilder, private _dialog: MatDialog,
    private _claimService: ClaimProfileService, private _route: ActivatedRoute,
    private _httpclient: HttpClient, private _ngzone: NgZone,
    private _router: Router) {

    this.ClaimProfileFormGroup = this._fb.group({
      regno: ['', Validators.required],
      docfirstname: ['', Validators.required],
      initialexpansion: ['', Validators.required],
      fathername: ['', Validators.required],
      regdate: ['', Validators.required],
      gender: ['', Validators.required],
      dob: ['', Validators.required],
      mobile: ['', Validators.required],
      email: ['', Validators.required],
      landline: [''],
      address: ['', Validators.required],
      district: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      pincode: ['', Validators.required],
      qualificationArray: this._fb.array([]),
      specialisation: ['', Validators.required],
      statecode: ['', Validators.required],
      consfees: ['', Validators.required],
      freevisit: ['', Validators.required],
      // about: ['', Validators.required],
      education: ['', Validators.required],
      experience: [''],
      services: [''],
      awards: [''],
      registration: [''],
      membership: [''],
      instituteArray: this._fb.array([]),
      userImage: [''],
      isFollowappUser: ['']
    });

  }

  IsValidJSONString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  ngOnInit() {
    this._claimService.myMethod$.subscribe((data: any) => {
      this.userProfileData = data;
    });
  }

  triggerResize() {
    // Wait for changes to be applied, then trigger textarea resize.
    this._ngzone.onStable.pipe(take(1))
      .subscribe(() => this.autosize.resizeToFitContent(true));
  }

  dispalyUserImage(event: any) {
    const filesToUpload = <Array<File>>event.target.files;
    console.log(filesToUpload);
    console.log(event.target.files);
    this.urlForUserImageJSON = event.target.files;

    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: ProgressEvent) => {
        this.urlForUserImage = (<FileReader>event.target).result;
        $("#PreviewImage").attr("src", this.urlForUserImage);
        $("#PreviewImage").show();
        $("#removeImage").show();
      }
      reader.readAsDataURL(event.target.files[0]);

    }
  }

  removeUserImage() {
    $("#PreviewImage").attr('src', '');
    $("#PreviewImage").hide();
    $("#removeImage").hide();
    $("#fileupload").val('');
  }

  add_qualifications() {
    this.Array_len = (<FormArray>this.ClaimProfileFormGroup.controls.qualificationArray).length;
    this.control = <FormArray>this.ClaimProfileFormGroup.controls['qualificationArray'];
    this.addRow = this._fb.group({
      Qualification: [''],
      College: [''],
      University: [''],
      YearofCompletion: ['']
    });
    if (this.Array_len == 0) {
      this.control.push(this.addRow);
    } else {
      if ((<FormArray>this.ClaimProfileFormGroup.controls.qualificationArray).valid) {
        this.qualError = 0;

        this.control.push(this.addRow);
      }
      else {
        this.qualError = 1;
      }

    }
  }

  remove_qualification(index: any) {
    const control = <FormArray>this.ClaimProfileFormGroup.controls['qualificationArray'];
    control.removeAt(index);
  }
  add_institutes() {
    this.Array_len = (<FormArray>this.ClaimProfileFormGroup.controls.instituteArray).length;
    this.control = <FormArray>this.ClaimProfileFormGroup.controls['instituteArray'];
    this.addInsRow = this._fb.group({
      clinicName: [''],
      location: [''],
      availability: ['']
      // fees: ['', Validators.required]
      // profile: ['']
    });
    if (this.Array_len == 0) {
      this.control.push(this.addInsRow);
    } else {
      if ((<FormArray>this.ClaimProfileFormGroup.controls.instituteArray).valid) {
        this.insError = 0;

        this.control.push(this.addInsRow);
      }
      else {
        this.insError = 1;
      }

    }
  }

  remove_institutes(index: any) {
    const control = <FormArray>this.ClaimProfileFormGroup.controls['instituteArray'];
    control.removeAt(index);
  }

  findLogoPath(event: any): void {
    console.log(event);
  }

  
  // Change the date format
  changeFormat(current_Date: any) {
    let currentDate = new Date(current_Date);
    let day = currentDate.getDate();
    let month = currentDate.getMonth() + 1;
    let year = currentDate.getFullYear();
    return year + '-' + ('00' + month).slice(-2) + '-' + ('00' + day).slice(-2);
  }



  convertByteArray(str) {
    let bytes = [];
    for (var i = 0; i < str.length; ++i) {
      var code = str.charCodeAt(i);
      
      bytes = bytes.concat([code]);
      
    }
    console.log();
  }


  SubmitForm(valid: any, value: any) {
    if (valid) {
      console.log(value);
      const formData: FormData = new FormData();
      formData.append("pID", '');
      formData.append("pRegNo", value.regno);
      formData.append("pRegDate", this.changeFormat(value.regdate));
      formData.append("pDoctorName", value.docfirstname);
      formData.append("pInitialExpansion", value.initialexpansion);
      formData.append("pFatherName", value.fathername);
      formData.append("pAddress", value.address);
      formData.append("pState", value.state);
      formData.append("pDistrict", value.district);
      formData.append("pCity", value.city);
      formData.append("pPincode", value.pincode);
      formData.append("pContactNo", value.mobile);

      var Qualification = {};
      var EducationalInfo = [];
       for(let i =0; i < value.qualificationArray.length; i++) {
         EducationalInfo.push(value.qualificationArray[i])
       }
       Qualification['EducationalInfo'] = EducationalInfo;
       console.log(Qualification);
      formData.append("pQualification", JSON.stringify(Qualification));

      const files: any = this.urlForUserImageJSON;
      if(files == '' || files == undefined) {
        formData.append("pImagePath", '');
      }
      else{
        formData.append("pImagePath", files[0], files[0].name);
      }
       
     
      if(value.isFollowappUser == true) {
        formData.append("pIsFollowAppRegistered ", "1");
      }
      else{
        formData.append("pIsFollowAppRegistered ", "0");
      }     
    
      var additionalInfo = {};
      additionalInfo = value;
      formData.append("pDOctorInfo", JSON.stringify(additionalInfo));


      console.log(JSON.stringify(value.instituteArray));
      this._httpclient.post('https://dev.followapp.in:3000/InsertorUpdateIMADoctorRegistrationLive', formData).subscribe((data: any) => {
        if (data.status == 0) {
          location.reload();
        }
        else {
          console.log("Something went be wrong");
        }
      })
    }
  }
}



