import { Component, OnInit, Inject, NgZone, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ClaimProfileService } from '../services/claim-profile.service';
import * as $ from 'jquery';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { take } from 'rxjs/operators';
import * as CryptoJS from 'crypto-js';
import * as MessageDigest from 'angular-md5';


export interface DialogData {
  profile: any;
  hospital: string;
  address: string;
  timing: string;
  fees: string;
}

export interface Gender {
  value: string;
  viewValue: string;
}
export interface StateCodes {
  value: string;
  viewValue: string;
}
export interface FreeVisit {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-claim-profile',
  templateUrl: './claim-profile.component.html',
  styleUrls: ['./claim-profile.component.css']
})
export class ClaimProfileComponent implements OnInit {

  ClaimProfileFormGroup: FormGroup;
  Array_len: any;
  control: any;
  addRow: any;
  addInsRow: any;
  qualError: any = 0;
  insError: any = 0;
  urlForUserImageJSON: any;
  urlForUserImage: any;

  regid: any;
  doctorname: any;
  education: string[] = [];
  doctorimage: any;
  doctordetail: any;
  doctorid: string;

  // Dialog Variables
  profile: any;
  hospital: string;
  address: string;
  timing: string;
  fees: string;
  aeskey = "Na1am TECHNOBEES";

  EducationTextareaText: any;

  header = new HttpHeaders();

  genders: Gender[] = [
    { value: 'Male', viewValue: 'Male' },
    { value: 'Female', viewValue: 'Female' },
    { value: 'Others', viewValue: 'Others' }
  ];
  statecodes: StateCodes[] = [
    { value: 'IN-TN', viewValue: 'IN-TN' },
    { value: 'IN-KL', viewValue: 'IN-KL' },
    { value: 'IN-AP', viewValue: 'IN-AP' },
    { value: 'IN-BR', viewValue: 'IN-BR' },
    { value: 'IN-TG', viewValue: 'IN-TG' },
    { value: 'IN-PY', viewValue: 'IN-PY' }
  ];
  freevisits: FreeVisit[] = [
    { value: '1', viewValue: '1' },
    { value: '2', viewValue: '2' },
    { value: '3', viewValue: '3' },
    { value: '4', viewValue: '4' },
    { value: '5', viewValue: '5' },
    { value: '6', viewValue: '6' },
    { value: '7', viewValue: '7' }
  ];

  public userProfileData: any;

  @ViewChild('autosize') autosize: CdkTextareaAutosize;

  constructor(private _fb: FormBuilder, private _dialog: MatDialog,
    private _claimService: ClaimProfileService, private _route: ActivatedRoute,
    private _httpclient: HttpClient, private _ngzone: NgZone,
    private _router: Router) {

    this.ClaimProfileFormGroup = this._fb.group({
      regno: [''],
      docfirstname: ['', Validators.required],
      doclastname: [''],
      gender: ['', Validators.required],
      dob: ['', Validators.required],
      mobile: [''],
      email: ['', Validators.required],
      landline: [''],
      password: ['', Validators.required],
      conpassword: ['', Validators.required],
      address: ['', Validators.required],
      pincode: ['', Validators.required],
      qualification: ['', Validators.required],
      // qualificationArray: this._fb.array([]),
      specialisation: ['', Validators.required],
      statecode: ['', Validators.required],
      consfees: ['', Validators.required],
      freevisit: ['', Validators.required],
      // about: ['', Validators.required],
      education: ['', Validators.required],
      experience: [''],
      services: [''],
      awards: [''],
      registration: [''],
      membership: [''],
      instituteArray: this._fb.array([]),
      userImage: ['']
    });

    // this.add_qualifications();
    // this.add_institutes();
    if (this._route.snapshot.params.regno) {
      // this.regid = this._route.snapshot.params.regno;
      this.regid = CryptoJS.AES.decrypt(this._route.snapshot.params.regno, 'FollowAPP').toString(CryptoJS.enc.Utf8);
      console.log(this._route.snapshot.params.regno);

    }
    else {
      this.regid = '';
    }
    if (this._route.snapshot.params.id) {
      console.log(this._route.snapshot.params.id);
      this.doctorid = CryptoJS.AES.decrypt(this._route.snapshot.params.id, 'FollowAPP').toString(CryptoJS.enc.Utf8);
      // this.doctorid = this._route.snapshot.params.id;
    }
    else {
      this.doctorid = '';
    }

    // this.enc("pa55w0rd");

    this._httpclient.post('https://dev.followapp.in:3000/GetIMADoctorDetailsOnRegisterNumberLive', { 'pRegNo': this.regid, 'pDoctorID': this.doctorid }).subscribe((res: any) => {

      // this._httpclient.post('https://followapp.in:5000/GetIMADoctorDetailsOnRegisterNumber', { 'pRegNo': this.regid }).subscribe((res: any) => {
      if (res.status == 0) {

        this.doctorname = res.response.DoctorName
        // let educationArraytemp = res.response.Qualification != '' ? (this.IsValidJSONString(res.response.Qualification) ? JSON.parse(res.response.Qualification) : '') : '';
        let educationArraytemp = res.response.Qualification != '' ? (typeof JSON.parse(res.response.Qualification) == 'object' ? JSON.parse(res.response.Qualification) : '') : '';
        let educationArray = educationArraytemp.EducationalInfo;
        if (educationArray != '') {
          if (Array.isArray(educationArray)) {
            educationArray.forEach((edu: any) => {
              edu.College = edu.College != '' ? edu.College + ' - ' : '';
              edu.University = edu.University && edu.University.includes('University') ? edu.University : edu.University + " University"
              let list = `${edu.Qualification} - ${edu.College} ${edu.University}, ${edu.YearofCompletion}`;
              this.education.push(list)
            })
          }
          else {
            this.education.push(res.response.Qualification);
          }
        }
        let QualList = []
        // res.response.Qualification = JSON.parse(res.response.Qualification)
        res.response.Qualification = res.response.Qualification != '' ? (this.IsValidJSONString(res.response.Qualification) ? JSON.parse(res.response.Qualification) : res.response.Qualification) : '';

        if (res.response.Qualification != '') {
          if (res.response.Qualification.EducationalInfo) {
            res.response.Qualification.EducationalInfo.forEach((qu: any) => {
              QualList.push(qu.Qualification)
            })
          }
          else {
            QualList.push(res.response.Qualification)
          }
        }
        res.response.qual = QualList.join(", ")
        if (res.response.ImagePath && res.response.ImagePath.includes('https')) {
          this.doctorimage = res.response.ImagePath
        } else {
          if (res.response.ImagePath) {
            this.doctorimage = `https://nalamhcit.com/imadoctorsimages/images/${res.response.ImagePath}`
          } else {
            this.doctorimage = "https://nalamhcit.com/imadoctorsimages/doctor_icon.jpg"
          }
        }
        res.response.Address = res.response.Address ? res.response.Address : '';
        this.doctordetail = res.response;

        var eduLength = this.education.length;

        if (eduLength > 0) {
          for (let i = 0; i < eduLength; i++) {
            if (i == 0) {
              this.EducationTextareaText = this.education[i];
            }
            else {
              this.EducationTextareaText = this.EducationTextareaText + "\n" + this.education[i];
            }
          }
        }
        console.log(this.EducationTextareaText);


      }
    })


  }

  IsValidJSONString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  ngOnInit() {
    this._claimService.myMethod$.subscribe((data: any) => {
      this.userProfileData = data;
    });
  }

  triggerResize() {
    // Wait for changes to be applied, then trigger textarea resize.
    this._ngzone.onStable.pipe(take(1))
      .subscribe(() => this.autosize.resizeToFitContent(true));
  }

  dispalyUserImage(event: any) {
    const filesToUpload = <Array<File>>event.target.files;
    console.log(filesToUpload);
    console.log(event.target.files);
    this.urlForUserImageJSON = event.target.files;

    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: ProgressEvent) => {
        this.urlForUserImage = (<FileReader>event.target).result;
        $("#PreviewImage").attr("src", this.urlForUserImage);
        $("#PreviewImage").show();
        $("#removeImage").show();
      }
      reader.readAsDataURL(event.target.files[0]);

    }
  }

  removeUserImage() {
    $("#PreviewImage").attr('src', '');
    $("#PreviewImage").hide();
    $("#removeImage").hide();
    $("#fileupload").val('');
  }

  add_qualifications() {
    this.Array_len = (<FormArray>this.ClaimProfileFormGroup.controls.qualificationArray).length;
    this.control = <FormArray>this.ClaimProfileFormGroup.controls['qualificationArray'];
    this.addRow = this._fb.group({
      Qualification: ['', Validators.required],
      College: [''],
      University: [''],
      YearofCompletion: ['', Validators.required]
    });
    if (this.Array_len == 0) {
      this.control.push(this.addRow);
    } else {
      if ((<FormArray>this.ClaimProfileFormGroup.controls.qualificationArray).valid) {
        this.qualError = 0;

        this.control.push(this.addRow);
      }
      else {
        this.qualError = 1;
      }

    }
  }

  remove_qualification(index: any) {
    const control = <FormArray>this.ClaimProfileFormGroup.controls['qualificationArray'];
    control.removeAt(index);
  }
  add_institutes() {
    this.Array_len = (<FormArray>this.ClaimProfileFormGroup.controls.instituteArray).length;
    this.control = <FormArray>this.ClaimProfileFormGroup.controls['instituteArray'];
    this.addInsRow = this._fb.group({
      clinicName: [''],
      location: [''],
      availability: ['']
      // fees: ['', Validators.required]
      // profile: ['']
    });
    if (this.Array_len == 0) {
      this.control.push(this.addInsRow);
    } else {
      if ((<FormArray>this.ClaimProfileFormGroup.controls.instituteArray).valid) {
        this.insError = 0;

        this.control.push(this.addInsRow);
      }
      else {
        this.insError = 1;
      }

    }
  }

  remove_institutes(index: any) {
    const control = <FormArray>this.ClaimProfileFormGroup.controls['instituteArray'];
    control.removeAt(index);
  }

  findLogoPath(event: any): void {
    console.log(event);
  }

  openDialog(): void {
    const dialogRef = this._dialog.open(AddConsultingInstituteDialog, {
      width: '450px',
      data: {
        profile: this.profile,
        hospital: this.hospital,
        address: this.address,
        timing: this.timing,
        fees: this.fees
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  // Change the date format
  changeFormat(current_Date: any) {
    let currentDate = new Date(current_Date);
    let day = currentDate.getDate();
    let month = currentDate.getMonth() + 1;
    let year = currentDate.getFullYear();
    return year + '-' + ('00' + month).slice(-2) + '-' + ('00' + day).slice(-2);
  }



  convertByteArray(str) {
    let bytes = [];
    for (var i = 0; i < str.length; ++i) {
      var code = str.charCodeAt(i);
      
      bytes = bytes.concat([code]);
      
    }
    console.log();
  }


  SubmitForm(valid: any, value: any) {
    if (valid) {
      const formData: FormData = new FormData();

      // var encryptedPassword = '';
      // this._httpclient.post('https://followapp.in:5000/DoctorClaimPasswordEncryption', {pPassword: value.password }).subscribe((data: any) => {
      //   if (data.status == 0) {
      //     encryptedPassword = data.response;
      //   }
      //   else {
      //     console.log("Something went be wrong");
      //   }
      // })
      // var cipher = CryptoJS.MD5.
      formData.append("pDoctorID", this.doctorid);
      formData.append("pUserName", this.doctordetail.Username);
      formData.append("pPassword", value.password);
      formData.append("pFirstName", value.docfirstname);
      formData.append("pLastName", value.doclastname);
      formData.append("pDOB", this.changeFormat(value.dob));
      formData.append("pGender", value.gender);
      formData.append("pPincode", value.pincode);
      formData.append("pRegion", value.address);
      formData.append("pQualification", value.qualification);
      formData.append("pAddress", value.address);
      formData.append("pSpecialisationID", '');
      formData.append("pSpecialisation", value.specialisation);
      formData.append("pMobileNumber", this.doctordetail.ContactNo);
      formData.append("pPhoneNumber", value.landline);
      formData.append("pEmailID", value.email);
      if (this.regid != '') {
        formData.append("pRegNumber", this.regid);
      }
      else {
        formData.append("pRegNumber", value.regno);
      }
      formData.append("pStateCode", value.statecode);
      const files: any = this.urlForUserImageJSON;
      if(files == '' || files == undefined) {
        formData.append("pPhoto", '');
      }
      else{
        formData.append("pPhoto", files[0], files[0].name);
      }
      formData.append("pEducation", value.education);
      formData.append("pExperience", value.experience);
      formData.append("pServices", value.services);
      formData.append("pAwards", value.awards);
      formData.append("pRegistrations", value.registration);
      formData.append("pMemberShips", value.membership);
      formData.append("pEmailVerificationCode", '');
      formData.append("pMobilePassCode", '');
      formData.append("pLanguagePreference", "Tamil");
      formData.append("pOnlineConsultationCharges", value.consfees);
      formData.append("pServiceChargeAmount", '');
      formData.append("pServicePercentage", '');
      formData.append("pCheckOutPreferenceFromWalletToAccount", "Monthly");
      formData.append("pPromotionalSMSAlert", "1");
      formData.append("pSMSAlertOnIndividualPatientAppointment", "1");
      formData.append("pFreeReviewVisitDaysCount", value.freevisit);
      formData.append("pConsultingInstituitions", JSON.stringify(value.instituteArray));
      formData.append("pLastOnline", '');
      formData.append("pUpdatedBy", this.doctordetail.Username);
      formData.append("pIsEmailVerified", "1");
      formData.append("pIsMobileNumberVerified", "1");
      formData.append("pIsActive", "1");
      formData.append("pOnlineAvailablity", '');

      var TempObj = {
        pDoctorID: null, pUserName: null,
        pPassword: value.password, pFirstName: value.docfirstname,
        pLastName: value.doclastname, pDOB: this.changeFormat(value.dob),
        pGender: valid.gender, pPincode: value.pincode, pRegion: null,
        pQualification: value.qualification, pAddress: value.address,
        pSpecialisationID: null, pSpecialisation: value.specialisation,
        pMobileNumber: this.doctordetail.ContactNo, pPhoneNumber: value.landline,
        pEmailID: value.email, pRegNumber: this.regid, pStateCode: value.statecode,
        pPhoto: this.urlForUserImageJSON, pEducation: value.education,
        pExperience: value.experience, pServices: value.service,
        pAwards: value.awards, pRegistrations: value.registration,
        pMemberShips: value.membership, pEmailVerificationCode: null,
        pMobilePassCode: null, pLanguagePreference: "Tamil",
        pOnlineConsultationCharges: value.consfees, pServiceChargeAmount: null,
        pServicePercentage: null, pCheckOutPreferenceFromWalletToAccount: "Monthly",
        pPromotionalSMSAlert: 1, pSMSAlertOnIndividualPatientAppointment: 1,
        pFreeReviewVisitDaysCount: value.freevisit,
        pConsultingInstituitions: value.instituteArray,
        pLastOnline: null, pUpdatedBy: null, pIsEmailVerified: null,
        pIsMobileNumberVerified: 1, pIsActive: 1, pOnlineAvailablity: null
      };
     
      // console.log(JSON.stringify(value.instituteArray));
      this._httpclient.post('https://followapp.in:5000/UpdateDoctorClaimDetails', formData).subscribe((data: any) => {
        if (data.status == 0) {
          this._router.navigate(['/profile', this.regid]);
        }
        else {
          console.log("Something went be wrong");
        }
      })
    }
  }
}

@Component({
  selector: 'consulting-institute-dialog',
  templateUrl: 'consulting-institute-dialog.html',
})
export class AddConsultingInstituteDialog {

  constructor(
    public dialogRef: MatDialogRef<AddConsultingInstituteDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }


}
