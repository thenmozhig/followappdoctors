import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MatSidenav } from '@angular/material';
import * as $ from 'jquery';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ClaimProfileService } from '../app/services/claim-profile.service';


// import { MouseEvent } from '@agm/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'followappadmin';
  opened: boolean = false;
  isSigninEnvironment: boolean = false;

  constructor(private _http: HttpClient, private _router: Router, private _service: ClaimProfileService) {

  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    $(window).scroll(function () {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        // if ($(this)[0].scrollY > 135) {
        $('#nhAdminToolbar').addClass('fixed');
      } else {
        $('#nhAdminToolbar').removeClass('fixed');
      }
    });


    (function () {
      if (window.history && window.history.pushState) {
        $(window).on('popstate', function () {

          var temp = window.location.pathname;
          if (temp == "/doctors") {
            $('#doctorSearchContainer').show();
            $("#profilecontainerDiv").hide();

          }
          else if ((temp.slice(0, -6)) == "/doctors/profile") {
            $('#doctorSearchContainer').hide();
            $("#profilecontainerDiv").show();

          }
          else if (temp == "/doctors") {
            $('#doctorSearchContainer').show();
            $("#profilecontainerDiv").hide();

          }
          else if ((temp.slice(0, -6)) == "/doctors/profile") {
            $('#doctorSearchContainer').hide();
            $("#profilecontainerDiv").show();

          }
          return;
        });
        $(window).on('load', function () {

          var temp = window.location.pathname;
          var tempPath = window.location.pathname.slice(0, -6)
          if (temp == "/doctors") {
            $('#doctorSearchContainer').show();
            $("#profilecontainerDiv").hide();

          }
          else if (tempPath == "/doctors/profile") {
            $('#doctorSearchContainer').hide();
            $("#profilecontainerDiv").show();

          }
          else if (temp == "/doctors") {
            $('#doctorSearchContainer').show();
            $("#profilecontainerDiv").hide();

          }
          else if (tempPath == "/doctors/profile") {
            $('#doctorSearchContainer').hide();
            $("#profilecontainerDiv").show();

          }
          return;
        });
      }
    })();


    $(document).on('click', '.viewIMADRProfile', function (e) {
      $("#doctorSearchContainer").hide();
      return;
    });


  }

  toggleAccordion(e) {
    if (e.target.getAttribute('isParent') == 'yes') {
      if (e.currentTarget.classList.contains("toggled")) {
        e.currentTarget.classList.remove('toggled')
      } else {
        e.currentTarget.classList.add('toggled')
      }
    }
  }

  toggleSideBar() {
    if (this.opened) {
      this.opened = false;
    } else {
      this.opened = true;
    }
  }
}





